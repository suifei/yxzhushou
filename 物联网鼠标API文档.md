# ESP8266/ESP32 物联网鼠标 API

![输入图片说明](mdimg/ios/%E7%89%A9%E8%81%94%E7%BD%91%E9%BC%A0%E6%A0%87.png)
![输入图片说明](mdimg/%E9%B1%BC%E5%8F%89%E5%8A%A9%E6%89%8B%E5%8E%9F%E7%90%86%E5%9B%BE.png)

##### 物联网鼠标简介
- 物联网鼠标采用[中国移动物联网平台](https://open.iot.10086.cn/) OneNET MQTT协议，开发者可通过HTTP POST/GET等协议，进行远程群发和单发命令控制ESP8266/ESP32 物联网芯片，芯片固件使用NodeMCU Lua 固件，可使用 Lua API代码进行鼠标移动、滚轮、单击/长按、滑动、键盘模拟输入等操作。
- 鼠标目前仅支持USB款（CH9329鼠标键盘一体化模块），蓝牙款（HX-03-HID-KM鼠标键盘一体化模块）两种协议，ESP8266 芯片仅支持蓝牙款，ESP32芯片可对不同鼠标类型来设置协议类型来进行操作，默认是USB款协议，ESP8266 芯片搭载蓝牙款鼠标适合新手测试和学习，工作室推荐ESP32芯片搭载USB款鼠标，稳定性更好。
- 物联网 ESP32 芯片，支持最多三个鼠标（USB/蓝牙），支持物联网（远程控制）和局域网（内网控制）同时使用或关闭，由于 OneNET 对于国内用户延迟较低（0.5-1秒），海外用户（包括香港/台湾）延迟较高（3秒以上），针对延迟低需求的用户可选择局域网（内网控制）。
- 国产 ESP8266/ESP32 物联网芯片拥有2.4G WIFI功能，目前暂时不支持连接5G路由器，WIFI信号对数量较多的环境下，抗干扰能力较差，为此ESP32 专门设计扩展以太网网口，可连接网线直接上网（无需设置WIFI账号密码，信号稳定）。
- 物联网鼠标支持WIndow、Mac、iOS、iPad、Android系统，iOS13.0以上才支持鼠标/键盘（推荐iOS13.5以上更稳定）。

##### 物联网鼠标款式(支持定制)
- （个人款）ESP8266（单核处理器） 无线WIFI + 蓝牙鼠标（支持一个） + 蓝牙键盘（支持一个）
	-  [ESP8266 蓝牙鼠标接线说明文档（点击查看） (wolai.com)](https://www.wolai.com/renyu/9jRXivxm5Yerp5DoSj76vB)
	- 优点：调试方便，成本最低，适合新手使用和开发
	- 缺点：不适合工作室，数量太多WIFI和蓝牙都容易干扰 
	- 功能：WIFI(2.4G)登录、物联网模式(远程控制)、自动接入物联网后台云管理、支持商业激活码激活、最多支持一对二个鼠标控制（两台手机）
 
- （工作室款）ESP32（双核处理器）  有线以太网/无线WIFI + USB鼠标（支持三个） + USB键盘（支持三个） 
	-  [ESP32 USB鼠标接线说明书（点击查看） (wolai.com)](https://www.wolai.com/renyu/8Rv1wRvm9kUHkRoKQcck65)
	- 优点：功能丰富，工作室自动化最佳方案，手机和ESP32使用以太网信号稳定
	- 缺点：成本高，适合有USB以太网二合一转换器的手机
	- 功能：WIFI(2.4G)登录/以太网(100M/10M)登录、物联网模式(远程控制)/局域网模式、自动接入物联网后台云管理、支持商业激活码激活、最多支持一对三个鼠标控制（三台手机）
  
- 原理讲解
	- [脚本免越狱 + 物联网技术方案介绍](https://www.bilibili.com/video/BV16341177dg?spm_id_from=333.999.0.0)
	- [免越狱稳定批量上线 - 物联网+USB鼠标+以太网+iOS转接口+App技术实现](https://www.bilibili.com/video/BV1gP4y1c7HZ?spm_id_from=333.999.0.0)
	- [更多内容正在更新中...]()

##### 机器配置要求
- IOS系统：iOS13以上
- iPad系统：无限制
- 安卓系统：无限制
- Window系统：无限制
- Mac系统：无限制

##### 新手常见问题

问题一：系统13.5以上，但是点击位置不准确

答：部分iOS手机为圆边，初始化位置并非（0，0），需要在绝对坐标上进行重新偏移计算。

图片来自 iPhone 13 pro max

![输入图片说明](mdimg/iPhone13.png)

![输入图片说明](mdimg/%E9%BC%A0%E6%A0%87%E5%88%9D%E5%A7%8B%E5%8C%96%E9%97%AE%E9%A2%98.png)

iPhone 13 pro max 鼠标相对移动参考示例

```lua
local py_x,py_y = 32,32
MouseMove({x = -3000,y = -3000}); --初始化(32,32)
MouseMove({x = 500 - py_x,y = 200 - py_y}); --减去圆边偏移坐标，实际到达 (500,200)
```

问题二：鼠标移动正常，但是手机点击无效

答：必须打开辅助触控功能(iOS悬浮窗)

问题三：连接鼠标后，鼠标操作出现偶尔失灵，点击无效

答：物联网后台取消订阅功能，可增加灵敏度

```lua
setMQTT({topic = {}}); --取消订阅可提高灵敏度
```

问题四：ESP32 串口日志不断重启，输出  `Brownout detector was triggered` 

答：电脑USB口供电不足，导致ESP32无法正常启动，烧录线不连接5v，直接对ESP32 DC电源供电（接入以太网调试也对供电要求更高）。

问题五：ESP32 以太网模式串口日志不断重启，输出  
`system_api: Base MAC address is not set, read default base MAC address from BLK0 of EFUSE[0m
[0;31mE (2748) emac: Timed out waiting for PHY register 0x2 to have value 0x0007(mask 0xffff). Current value 0x0000[0m
[0;31mE (3748) emac: Timed out waiting for PHY register 0x3 to have value 0xc0f0(mask 0xfff0). Current value 0x0000[0m
[0;31mE (3748) emac: Initialise PHY device Timeout[0m
PANIC: unprotected error in call to Lua API (esp_eth_enable failed)` 

`abort() was called at PC 0x4013481f on core 0`

`ELF file SHA256: 8aa875ffcf9d57f8`

`Backtrace: 0x400868cb:0x3ffbc9c0 0x40086be5:0x3ffbc9e0 0x4013481f:0x3ffbca00 0x40136bb7:0x3ffbca20 0x40130ee1:0x3ffbca40 0x4013803d:0x3ffbca60 0x40133f7b:0x3ffbca80 0x400d8a31:0x3ffbcad0 0x4013710f:0x3ffbcb30 0x4013586d:0x3ffbcb70 0x401371e6:0x3ffbcbc0 0x40137e99:0x3ffbcbe0 0x400d99f1:0x3ffbcc00 0x400f67ed:0x3ffbcc20 0x400f95a7:0x3ffbcc50 0x400d20cf:0x3ffbcc90`

`Rebooting...`

答：拔出网线，再重新通电，等待以太网口黄灯亮起时，再接入网线，如果尚未解决，请切换到 WIFI 模式，再重复上面操作。（WIFI 模式切换以太网模式时，网线必须切换成功黄灯亮起才能插入，如果提前插入可能会导致该错误）

ESP32 物联网芯片：
```lua
setWIFI({WIFI = true}); --启动WIFI模式，关闭以太网模式
```


##### 新手指导

[电脑串口工具下载](https://gitee.com/lua_development/yxzhushou/tree/master/%E7%89%A9%E8%81%94%E7%BD%91%E9%BC%A0%E6%A0%87%E7%94%B5%E8%84%91%E8%B0%83%E8%AF%95%E5%B7%A5%E5%85%B7) 

* 【必看】iOS手机鼠标显示、指针跟踪速度设置 [图文教程](#ios鼠标设置显示)  [视频教程](https://www.bilibili.com/video/BV1EF411E7HN/?spm_id_from=333.788)
* `版本号：ESP8266 v1.0.5` 【必看】OneNET 中国移动物联网平台后台管理设置 [视频教程](https://www.bilibili.com/video/BV1LR4y157Ti?spm_id_from=333.999.0.0)
*  `版本号：ESP8266 v1.0.3` 【必看】物联网鼠标电脑串口调试 [视频教程](https://www.bilibili.com/video/BV1vP4y1w7xU/?spm_id_from=333.788) 
*  `版本号：ESP8266 v1.0.3` iOS快捷指令Post控制 [HTTPpost示例](#httppost)  [视频教程](https://www.bilibili.com/video/BV1Lb4y147v4?spm_id_from=333.999.0.0)
*  `版本号：ESP8266 v1.0.3` 网页端在线Post控制 [HTTPpost示例](#httppost)  [视频教程](https://www.bilibili.com/video/BV1G44y1p7Q6/?spm_id_from=333.788)
*  `版本号：ESP8266 v1.0.3` iOS13.3.1 鼠标点击不准确解决方案 [视频教程](https://www.bilibili.com/video/BV1oU4y1f7gb/?spm_id_from=333.788)
*  【必看】鱼叉助手App(iOS免越狱)  [HTTPpost示例](#httppost) [视频教程一](https://www.bilibili.com/video/BV1bi4y127p6?spm_id_from=333.999.0.0) [视频教程二](https://www.bilibili.com/video/BV1nu411X7fw?spm_id_from=333.999.0.0)
*  触动精灵App(越狱)  [HTTPpost示例](#httppost) [视频教程](https://www.bilibili.com/video/BV1Jq4y1879G?spm_id_from=333.999.0.0)
*  `版本号：ESP32 v1.0.5`  关闭日志功能（串口通讯修改为鼠标模式，可控制 0 号鼠标）[关闭日志功能](#setuart)
*  `版本号：ESP32 v1.0.5`  iOS 物联网 ESP32 USB鼠标一对三 苹果手机使用教程 [视频教程](https://www.bilibili.com/video/BV1f341157Dj?spm_id_from=333.999.0.0)
* `版本号：ESP32 v1.0.55` Android 物联网 ESP32 一对三 安卓手机使用教程 [视频教程](https://www.bilibili.com/video/BV1yB4y137LK?spm_id_from=444.41.list.card_archive.click&vd_source=d9bc656fe24b23075edb631fa18d3e0c)

##### 技术支持：
* 反馈问题使用 [码云issues](https://gitee.com/lua_development/yxzhushou/issues) 留言，我们会关注和回复。
* 更新日志/bug修复 [ESP8266更新日志](#ESP8266更新日志)  [ESP32更新日志](#ESP32更新日志) 
* 人鱼情未了 QQ：2787283623
* 物联网交流 QQ群：715467974

##### 鼠标函数：
* [MouseMove](#mousemove) 鼠标移动(相对坐标)
* [MouseDown](#mousedown) 鼠标按下
* [MouseUp](#mouseup) 鼠标抬起
* [MouseClick](#mouseclick) 鼠标单击/鼠标长按
* [MouseSlide](#mouseslide) 鼠标滑动
* [touchMove](#touchmove) 鼠标移动(绝对坐标)

##### 键盘函数：
* [KeyboardClick](#keyboardclick) 键盘输入

##### 设置函数：
* [setWIFI](#setwifi) 设置WIFI配置
* [setMQTT](#setmqtt) 设置MQTT配置
* [setKEY](#setkey) 设置激活码
* [setUART](#setuart) 设置鼠标配置

##### 获取函数：
* [getWIFI](#getwifi) 获取WIFI配置
* [getMQTTcode](#getmqttcode) 获取物联网平台ID
* [getMQTT](#getmqtt) 获取MQTT配置
* [getUART](#getuart) 获取鼠标配置
* [getESP](#getesp) 获取ESP8266配置

##### 其他函数：
* [ptable](#ptable) 打印表
* [node.restart()](#node.restart) 重启设备

# MouseMove

### **鼠标移动(相对坐标)**

函数说明：从当前鼠标位置开始偏移像素点

玩法说明：一对一鼠标无需设置 io，一对二、一对三需要设置 io 进行信号输出切换

 `版本号：ESP8266 v1.0.3` ESP8266物联网鼠标电脑串口调试 [视频教程](https://www.bilibili.com/video/BV1vP4y1w7xU/?spm_id_from=333.788) 

参数|类型|说明
-|-|-
io|<font color=#00ffff>number</font>|鼠标 io 口（默认：1），ESP8266（1），ESP32（0/1/2）[io说明](#物联网鼠标款式支持定制)
x|<font color=#00ffff>number</font>|水平相对移动 (负数 = 向左，正数 = 向右)
y|<font color=#00ffff>number</font>|垂直相对移动 (负数 = 向上，正数 = 向下)
z|<font color=#00ffff>number</font>|滚轮滚动 (负数 = 向上，正数 = 向下) [滚轮演示教程](https://www.bilibili.com/video/BV1dF411g749?spm_id_from=333.999.0.0)
Proportion_x|<font color=#00ffff>number</font>| `版本号：ESP32 v1.0.55` x 坐标指针移动比例值（公式：X × 比例值 = 最终移动坐标）
Proportion_y|<font color=#00ffff>number</font>| `版本号：ESP32 v1.0.55` y 坐标指针移动比例值（公式：Y × 比例值 = 最终移动坐标）
Proportion_z|<font color=#00ffff>number</font>| `版本号：ESP32 v1.0.55` z 坐标指针移动比例值（公式：Z × 比例值 = 最终移动坐标）
system|<font color=#FF8C00>string</font>|`版本号：ESP32 v1.0.55` USB鼠标系统协议切换 "iOS"、"Android"（默认：”iOS“）

**串口通讯**

```lua
--ESP8266/ESP32 1号鼠标相对移动
MouseMove({x = -30,y = -30});

MouseMove({x = -30,y = -30, system = "Android"}); --安卓系统 相对移动

MouseMove({z = -30}); --滚轮向上滚动

--ESP32 0号鼠标相对移动
MouseMove({io = 0,x = -30,y = -30});

--ESP32 2号鼠标相对移动
MouseMove({io = 2,x = -30,y = -30});

--ESP8266 v1.0.57 / ESP32 v1.0.54 新增 滚轮功能
MouseMove({z = 20000});  --滚动屏幕

--ESP32 v1.0.55 新增比例值动态调参
--如设备默认x比例值2.083333，x比例改为 1，一般用于需要坐标精准点击，比例值不固定的手机系统，特别是安卓系统
MouseMove({x = -30,y = -30,Proportion_x = 1,Proportion_y = 1});
--
```

**远程设置 HTTP post 请求 body**

名称|说明
-|-
API|函数名称
data|参数(io,x,y,z)
data.io|`版本号：ESP32 v1.0.5` 鼠标 io 口（默认：1），ESP8266（1），ESP32（0/1/2）
Log|串口显示日志 = true(默认)，串口隐藏日志 = false(关闭可降低鼠标操作时干扰) [日志全局关闭](#setuart)

```JSON
{  
	"API":"MouseMove",  
	"data":{
		"x":-30,
		"y":-30
	}
}
```

```JSON
{  
	"API":"MouseMove",  
	"data":{
		"io":2,
		"x":-30,
		"y":-30
	}
}
```
# MouseDown

### **鼠标按下**

函数说明：一直按住不放，需要搭配 [MouseUp](#mouseup) 鼠标抬起

玩法说明：一对一鼠标无需设置 io，一对二、一对三需要设置 io 进行信号输出切换

参数|类型|说明
-|-|-
io|<font color=#00ffff>number</font>|`版本号：ESP32 v1.0.5` 鼠标 io 口（默认：1），ESP8266（1），ESP32（0/1/2）[io说明](#物联网鼠标款式支持定制)
click|<font color=#FF8C00>string</font>|`版本号：ESP32 v1.0.5` "左键"（默认）、"中键"（滚轮键）、"右键"
system|<font color=#FF8C00>string</font>|`版本号：ESP32 v1.0.55` USB鼠标系统协议切换 "iOS"、"Android"（默认：”iOS“）

**串口通讯**

```lua
--1号鼠标按住
MouseDown();

MouseDown({system = "Android"}); --安卓系统，只对USB鼠标生效，蓝牙鼠标无需设置

--0号鼠标按住
MouseDown({io = 0});

--2号鼠标按住
MouseDown({io = 2});
```

**远程设置 HTTP post 请求 body**

名称|说明
-|-
API|函数名称
data|参数(io,click)
data.io|`版本号：ESP32 v1.0.5` 鼠标 io 口（默认：1），ESP8266（1），ESP32（0/1/2）
Log|串口显示日志 = true(默认)，串口隐藏日志 = false(关闭可降低鼠标操作时干扰) [日志全局关闭](#setuart)

```JSON
{  
	"API":"MouseDown"
}
```

```JSON
{  
	"API":"MouseDown",
	"data":{
		"io":2
	}
}
```

# MouseUp

### **鼠标抬起**

函数说明：抬起，需要搭配 [MouseDown](#mousedown) 鼠标按下

玩法说明：一对一鼠标无需设置 io，一对二、一对三需要设置 io 进行信号输出切换

参数|类型|说明
-|-|-
io|<font color=#00ffff>number</font>|`版本号：ESP32 v1.0.5` 鼠标 io 口（默认：1），ESP8266（1），ESP32（0/1/2）[io说明](#物联网鼠标款式支持定制)
system|<font color=#FF8C00>string</font>|`版本号：ESP32 v1.0.55` USB鼠标系统协议切换 "iOS"、"Android"（默认：”iOS“）

**串口通讯**

```lua
--1号鼠标抬起
MouseUp();

MouseUp({system = "Android"}); --安卓系统，只对USB鼠标生效，蓝牙鼠标无需设置

--0号鼠标抬起
MouseUp({io = 0});

--2号鼠标抬起
MouseUp({io = 2});
```

**远程设置 HTTP post 请求 body**

名称|说明
-|-
API|函数名称
data|参数(io)
data.io|`版本号：ESP32 v1.0.5` 鼠标 io 口（默认：1），ESP8266（1），ESP32（0/1/2）
Log|串口显示日志 = true(默认)，串口隐藏日志 = false(关闭可降低鼠标操作时干扰) [日志全局关闭](#setuart)

```JSON
{  
	"API":"MouseUp"
}
```

```JSON
{  
	"API":"MouseUp",
	"data":{
		"io":2
	}
}
```

# MouseClick

### **鼠标单击/鼠标长按**

函数说明：按下后开始计时，自动抬起

玩法说明：一对一鼠标无需设置 io，一对二、一对三需要设置 io 进行信号输出切换

参数|类型|说明
-|-|-
io|<font color=#00ffff>number</font>|`版本号：ESP32 v1.0.5` 鼠标 io 口（默认：1），ESP8266（1），ESP32（0/1/2）[io说明](#物联网鼠标款式支持定制)
sleep|<font color=#00ffff>number</font>|长按延时，时间结束自动调用 MouseUp 函数，单位：毫秒 (1000 = 1秒)
click|<font color=#FF8C00>string</font>|`版本号：ESP32 v1.0.5` "左键"（默认）、"中键"（滚轮键）、"右键"
system|<font color=#FF8C00>string</font>|`版本号：ESP32 v1.0.55` USB鼠标系统协议切换 "iOS"、"Android"（默认：”iOS“）

**串口通讯**

```lua
--1号鼠标单击/鼠标长按
MouseClick({sleep = 5000});--默认iOS系统 左键长按5秒

MouseClick({sleep = 5000, click = "右键"});--默认iOS系统 右键长按5秒

MouseClick({sleep = 5000, system = "Android"});--安卓系统 长按5秒

--0号鼠标单击/鼠标长按
MouseClick({io = 0, sleep = 5000});--5秒

--2号鼠标单击/鼠标长按
MouseClick({io = 2, sleep = 5000});--5秒
```

**远程设置 HTTP post 请求 body**

名称|说明
-|-
API|函数名称
data|参数（io, sleep, click）
data.io|`版本号：ESP32 v1.0.5` 鼠标 io 口（默认：1），ESP8266（1），ESP32（0/1/2）
Log|串口显示日志 = true(默认)，串口隐藏日志 = false(关闭可降低鼠标操作时干扰) [日志全局关闭](#setuart)

```JSON
{  
	"API":"MouseClick",  
	"data":{
		"sleep":5000
	},
	"Log":false
}
```

```JSON
{  
	"API":"MouseClick",  
	"data":{
		"io":2,
		"sleep":5000
	},
	"Log":false
}
```

# MouseSlide

### **鼠标滑动**

函数说明：x,y 起点坐标，x1,y1 终点坐标

玩法说明：一对一鼠标无需设置 io，一对二、一对三需要设置 io 进行信号输出切换

参数|类型|说明
-|-|-
io|<font color=#00ffff>number</font>|`版本号：ESP32 v1.0.5` 鼠标 io 口（默认：1），ESP8266（1），ESP32（0/1/2）[io说明](#物联网鼠标款式支持定制)
x|<font color=#00ffff>number</font>|起点x
y|<font color=#00ffff>number</font>|起点y
x1|<font color=#00ffff>number</font>|终点x
y1|<font color=#00ffff>number</font>|终点y
click|<font color=#FF8C00>string</font>|`版本号：ESP32 v1.0.5` "左键"（默认）、"中键"（滚轮键）、"右键"
INIT_X|<font color=#00ffff>number</font>|初始化x位置(默认：-3000)
INIT_Y|<font color=#00ffff>number</font>|初始化y位置(默认：-3000)
MouseMove_sleep|<font color=#00ffff>number</font>|相对移动前延时(默认：200，单位：毫秒 (1000 = 1秒))
MouseDown_sleep|<font color=#00ffff>number</font>|鼠标按下前延时(默认：50，单位：毫秒 (1000 = 1秒))
MouseSlide_sleep|<font color=#00ffff>number</font>|滑动前延时(默认：50，单位：毫秒 (1000 = 1秒))
MouseUp_sleep|<font color=#00ffff>number</font>|鼠标抬起前延长(默认：100，单位：毫秒 (1000 = 1秒))
Proportion_x|<font color=#00ffff>number</font>| `版本号：ESP32 v1.0.55` x 坐标指针移动比例值（公式：X × 比例值 = 最终移动坐标）
Proportion_y|<font color=#00ffff>number</font>| `版本号：ESP32 v1.0.55` y 坐标指针移动比例值（公式：Y × 比例值 = 最终移动坐标）
Proportion_z|<font color=#00ffff>number</font>| `版本号：ESP32 v1.0.55` z 坐标指针移动比例值（公式：Z × 比例值 = 最终移动坐标）
system|<font color=#FF8C00>string</font>|`版本号：ESP32 v1.0.55` USB鼠标系统协议切换 "iOS"、"Android"（默认：”iOS“）

**函数原型（仅供参考）：**

```lua
--仅供参考，请勿直接复制使用
function MouseSlide(t)
	local io = t.io or 1 --默认1号鼠标
	local MouseMove_sleep = t.MouseMove_sleep or 200  --初始化前延时
	local MouseDown_sleep = t.MouseDown_sleep or 50  --初始化后延时
	local MouseSlide_sleep = t.MouseSlide_sleep or 50  --到达指定位置后延时
	local MouseUp_sleep = t.MouseUp_sleep or 100
	
	MouseMove({io = io, x = t.INIT_X or -3000,y = t.INIT_Y or -3000}); --初始化到屏幕左上角
	sleep(MouseMove_sleep) --延时
	MouseMove({io = io, x = t.x,y = t.y}); --相对移动 100,100
	sleep(MouseDown_sleep) --延时
	MouseDown({io = io, click = "左键" }); --按下
	sleep(MouseSlide_sleep) --延时
	--这里根据距离，可拆分多个MouseMove进行滑动，一次性滑动距离过大会导致滑动距离不精确。
	MouseMove({io = io, x = t.x1 - t.x,y = t.y1 - t.y}); --相对移动 200-100, 200-100
	sleep(MouseUp_sleep) --延时
	MouseUp({io = io}); --松手
end

MouseSlide({x = 100, y = 100, x1 = 200, y1 = 200}) --调用函数

```

**串口通讯**

```lua
--1号鼠标滑动
MouseSlide({x = 365, y = 867, x1 = 370, y1 = 266}); --默认 iOS系统 滑动

MouseSlide({x = 365, y = 867, x1 = 370, y1 = 266, system = "Android"}); --安卓系统滑动

--0号鼠标滑动
MouseSlide({io = 0, x = 365, y = 867, x1 = 370, y1 = 266}); --滑动

--2号鼠标滑动
MouseSlide({io = 2, x = 365, y = 867, x1 = 370, y1 = 266}); --滑动
```

**远程设置 HTTP post 请求 body**

名称|说明
-|-
API|函数名称
data|参数（io,x,y,x1,y1,click）
data.io|`版本号：ESP32 v1.0.5` 鼠标 io 口（默认：1），ESP8266（1），ESP32（0/1/2）
Log|串口显示日志 = true(默认)，串口隐藏日志 = false(关闭可降低鼠标操作时干扰) [日志全局关闭](#setuart)

```JSON
{  
	"API":"MouseSlide",  
	"data":{
		"x":365,
		"y":867,		
		"x1":370,
		"y1":266
	},
	"Log":false
}
```

```JSON
{  
	"API":"MouseSlide",  
	"data":{
		"io":2,
		"x":365,
		"y":867,		
		"x1":370,
		"y1":266
	},
	"Log":false
}
```

# touchMove

### **鼠标移动(绝对坐标)**

函数说明：初始化鼠标位置到左上角后开始偏移像素点

玩法说明：一对一鼠标无需设置 io，一对二、一对三需要设置 io 进行信号输出切换

参数|类型|说明
-|-|-
io|<font color=#00ffff>number</font>|`版本号：ESP32 v1.0.5` 鼠标 io 口（默认：1），ESP8266（1），ESP32（0/1/2）[io说明](#物联网鼠标款式支持定制)
x|<font color=#00ffff>number</font>|水平绝对移动
y|<font color=#00ffff>number</font>|垂直绝对移动
click|<font color=#FF8C00>string</font>|`版本号：ESP32 v1.0.5` "左键"（默认）、"中键"（滚轮键）、"右键"
INIT_X|<font color=#00ffff>number</font>|初始化x位置(默认：-3000)
INIT_Y|<font color=#00ffff>number</font>|初始化y位置(默认：-3000)
sleep|<font color=#00ffff>number</font>|相对移动前延时，等待指针初始化完毕后移动(默认：500，单位：毫秒 (1000 = 1秒))
Proportion_x|<font color=#00ffff>number</font>| `版本号：ESP32 v1.0.55` x 坐标指针移动比例值（公式：X × 比例值 = 最终移动坐标）
Proportion_y|<font color=#00ffff>number</font>| `版本号：ESP32 v1.0.55` y 坐标指针移动比例值（公式：Y × 比例值 = 最终移动坐标）
Proportion_z|<font color=#00ffff>number</font>| `版本号：ESP32 v1.0.55` z 坐标指针移动比例值（公式：Z × 比例值 = 最终移动坐标）
system|<font color=#FF8C00>string</font>|`版本号：ESP32 v1.0.55` USB鼠标系统协议切换 "iOS"、"Android"（默认：”iOS“）

**函数原型（仅供参考）：**
```lua
function touchMove(t)
	local io = t.io or 1 --默认1号鼠标
	local s = t.sleep or 500  --相对移动前延时
	MouseMove({io = io, x = t.INIT_X or -3000, y = t.INIT_Y or -3000}); --默认初始化到屏幕左上角
	sleep(s) --延时
	--必须等待指针初始化完毕才可以操作，否则可能会移动不准确 
	MouseMove({io = io，x = t.x, y = t.y}); --相对移动
end
```

**串口通讯**

```lua
--1号鼠标移动
touchMove({x = 639, y = 1245});

touchMove({x = 639, y = 1245, system = "Android"}); --安卓系统 移动

touchMove({x = -100, y = -100, INIT_X = 3000, INIT_Y = 3000}); --改变初始化位置到右下角，向左上角相对移动100，100

--0号鼠标移动
touchMove({io = 0, x = 639, y = 1245});

--2号鼠标移动
touchMove({io = 2, x = 639, y = 1245});
```

**远程设置 HTTP post 请求 body**

名称|说明
-|-
API|函数名称
data|参数(x,y,click)
io|`版本号：ESP32 v1.0.5` 鼠标 io 口（默认：1），ESP8266（1），ESP32（0/1/2）
Log|串口显示日志 = true(默认)，串口隐藏日志 = false(关闭可降低鼠标操作时干扰) [日志全局关闭](#setuart)

```JSON
{  
	"API":"touchMove",  
	"data":{
		"x":100,
		"y":100
	}
}
```

```JSON
{  
	"API":"touchMove",  
	"data":{
		"io":2,
		"x":100,
		"y":100
	}
}
```

# KeyboardClick

### **键盘输入**

函数说明：模拟键盘输入，最多支持同时按下 6 个键

玩法说明：一对一鼠标无需设置 io，一对二、一对三需要设置 io 进行信号输出切换

参数|类型|参数1
-|-|-
io|<font color=#00ffff>number</font>|`版本号：ESP32 v1.0.5` 鼠标 io 口（默认：1），ESP8266（1），ESP32（0/1/2）[io说明](#物联网鼠标款式支持定制)
Keyboard1|<font color=#FF8C00>string</font>|键盘值（最多6个）
Keyboard2|<font color=#FF8C00>string</font>|键盘值（最多6个）
Keyboard3|<font color=#FF8C00>string</font>|键盘值（最多6个）
Keyboard4|<font color=#FF8C00>string</font>|键盘值（最多6个）
Keyboard5|<font color=#FF8C00>string</font>|键盘值（最多6个）
Keyboard6|<font color=#FF8C00>string</font>|键盘值（最多6个）

**串口通讯**

```lua
--1号鼠标输出（默认：1）
KeyboardClick({"k"}); --输入小写k
KeyboardClick({"K"}); --输入大写k
KeyboardClick({"Shift","k"}); --输入大写k
KeyboardClick({"Space"}); --输入空格键
KeyboardClick({"\\"}); --反斜杠 \
KeyboardClick({"\'"}); --单引号 '
KeyboardClick({"?"}); --问号
KeyboardClick({"Win","c"});  --Mac、iOS 复制
KeyboardClick({"Win","v"});  --Mac、iOS 粘贴
KeyboardClick({"Ctrl","c"});  --Windows 复制
KeyboardClick({"Ctrl","v"});  --Windows 粘贴

--0号鼠标输出
KeyboardClick({io = 0,"k"}); --输入小写k
KeyboardClick({io = 0,"K"}); --输入大写k
KeyboardClick({io = 0,"Shift","k"}); --输入大写k
KeyboardClick({io = 0,"Space"}); --输入空格键
KeyboardClick({io = 0,"\\"}); --反斜杠 \

--2号鼠标输出
KeyboardClick({io = 2,"\'"}); --单引号 '
KeyboardClick({io = 2,"?"}); --问号
KeyboardClick({io = 2,"Win","c"});  --Mac、iOS 复制
KeyboardClick({io = 2,"Win","v"});  --Mac、iOS 粘贴
KeyboardClick({io = 2,"Ctrl","c"});  --Windows 复制
KeyboardClick({io = 2,"Ctrl","v"});  --Windows 粘贴
```

**远程设置 HTTP post 请求 body**

名称|说明
-|-
API|函数名称
data|参数（Keyboard1,Keyboard2,Keyboard3,Keyboard4,Keyboard5,Keyboard6）
io|`版本号：ESP32 v1.0.5` 鼠标 io 口（默认：1），ESP8266（1），ESP32（0/1/2）
Log|串口显示日志 = true(默认)，串口隐藏日志 = false(关闭可降低鼠标操作时干扰) [日志全局关闭](#setuart)

```JSON
{  
	"API":"KeyboardClick",  
	"data":[
		"a","b","c","d","e"
	],
	"Log":false
}
```

```JSON
{  
	"API":"KeyboardClick",  
	"data":[
		"Shift","k"
	],
	"Log":false
	"io":2
}
```

**键盘值**

说明：部分键盘按键对手机可能失效，只对电脑有效，自行测试

参数|说明
-|-
a - z|小写字母(26个)
A - Z|大写字母(26个)
F1 - F12|12个
0 - 9|数字键(10个)
Enter|回车
Esc|取消（iOS解锁键）
Backspace|回退
Tab|制表符
Space|空格
CapsLock|大小写
PrintScreenSysrq|截图键
ScrollLock|滚动锁定键
PauseBreak|中断暂停键
Insert|插入键
Home|返回主界面
PageUp|向上翻页键
PageDown|向下翻页键
RightArrow|→ 右方向键
LeftArrow|← 左向键
DownArrow|↓ 下方向键
UpArrow|↑ 上方向键
Delete|删除键
End|光标定位键
Numlock|数字键盘开关键
Application|右侧跟win挨着的键
Ctrl|
Shift|
Alt|
Win|
+、-、*、/、=、!、@....|其他符号

# setWIFI

### **设置WIFI配置**

函数说明：使用该函数后会自动重启设备

玩法说明：
- 以太网模式只支持100M/10M网口，如果使用1000M网口，会导致ESP32重启，无法正常联网，芯片发热发烫。
- WIFI模式只支持2.4G，如果账号密码出错，则设备无法正常联网，如果账号密码正确，但是路由器是5G信号，则设备无法正常联网。

参数|类型|说明
-|-|-
version|<font color=#FF8C00>string</font>|版本号 WIFI
ssid|<font color=#FF8C00>string</font>|账号(不要使用中文)
pwd|<font color=#FF8C00>string</font>|密码(不要使用中文)
save|<font color=#00ff00>boolean</font>|保存账号密码到闪存(启动时自动连接速度快)
WIFI|<font color=#00ff00>boolean</font>|`版本号：ESP32 v1.0.5` 启动或关闭WIFI（启动以太网模式）
HTTPServer|<font color=#00ff00>boolean</font>|`版本号：ESP32 v1.0.5` 启动或关闭局域网服务（无法局域网内控制鼠标）
HTTPPost|<font color=#00ff00>boolean</font>|`版本号：ESP32 v1.0.5` 局域网监听端口(默认：80)
MQTTServer|<font color=#00ff00>boolean</font>|`版本号：ESP32 v1.0.5` 启动或关闭物联网服务（无法物联网远程控制鼠标）

**串口通讯 ：**

ESP8266 物联网芯片：

```lua
--ESP8266芯片 WIFI
setWIFI({ssid = "Zhanghao",pwd = "Mima"}); 
```

ESP32 物联网芯片：
```lua
--ESP32芯片 以太网/WIFI 双模式
setWIFI({ssid = "Zhanghao",pwd = "Mima",WIFI = true}); --账号密码不允许中文，启动WIFI模式

setWIFI({WIFI = false}); --关闭WIFI模式，启动以太网模式

setWIFI({HTTPServer = false}); --关闭局域网服务

setWIFI({MQTTServer = false}); --关闭物联网服务
```

**远程设置 HTTP post 请求 body：**

名称|说明
-|-
API|函数名称
data|参数

```JSON
{  
	"API":"setWIFI",  
	"data":{  
		"ssid":"Zhanghao",  
		"pwd":"Mima",
		"WIFI":true
	}
}
```

# setMQTT

### **设置MQTT配置**

函数说明：修改MQTT配置，使用该函数后会自动重启设备

玩法说明：对接中国移动物联网平台(OneNET)，实现后台管理，远程设置设备参数。

注册方法：OneNET 中国移动物联网平台后台管理设置 [视频教程](https://www.bilibili.com/video/BV1LR4y157Ti?spm_id_from=333.999.0.0)

设备支持远程的函数：[HTTPPost](#httppost)

参数|类型|说明
-|-|-
version|<font color=#FF8C00>string</font>|版本号 MQTT
MQTT_Host|<font color=#FF8C00>string</font>|MQTT地址
MQTT_Port|<font color=#00ffff>number</font>|MQTT端口
topic|<font color=#ffff00>table</font>|`版本号：ESP32 v1.0.5` 订阅主题
register_code|<font color=#FF8C00>string</font>|设备注册码
Master_Apikey|<font color=#FF8C00>string</font>|产品api-key
Master_ID|<font color=#FF8C00>string</font>|产品ID

**串口通讯**

```lua
--设置物联网后台，一键注册
setMQTT({register_code = "设备注册码",Master_Apikey = "产品api-key",Master_ID = "产品ID"});

setMQTT({topic = {}}); --取消订阅可提高鼠标灵敏度，测试有效
```

# setKEY

### **设置激活码**

函数说明：初始机无需设置，默认5年（支持商业定制，提供自定义生成激活时间方法），一机一码激活设备，使用该函数后会自动重启设备

玩法说明：可定制生成自定义时间激活码，发送给客户使用，捆绑脚本编程

参数|类型|说明
-|-|-
version|<font color=#FF8C00>string</font>|版本号
type|<font color=#FF8C00>string</font>|鼠标类型
BaudRate|<font color=#00ffff>number</font>|波特率
Proportion|<font color=#ffff00>table</font>|1号鼠标移动值比例 x(水平移动)、y(垂直移动)、z(滚轮转动)
Proportion_0|<font color=#ffff00>table</font>|0号鼠标移动值比例 x(水平移动)、y(垂直移动)、z(滚轮转动)
Proportion_2|<font color=#ffff00>table</font>|2号鼠标移动值比例 x(水平移动)、y(垂直移动)、z(滚轮转动)
KEY|<font color=#FF8C00>string</font>|设备激活码
TimeExpired|<font color=#FF8C00>string</font>|设备有效日期
Log|<font color=#00ff00>boolean</font>|全局日志模式 开启 = true/关闭 = false

串口通讯 Time Expired 参数说明：

参数|说明
-|-
Time Expired 20230210163500 |有效期截止时间 2023年2月10日16:35
Time Expired error！|激活码验证失败

![输入图片说明](mdimg/%E6%BF%80%E6%B4%BB%E7%A0%81%E9%AA%8C%E8%AF%81%E5%A4%B1%E8%B4%A5.png)

**串口通讯**

```lua
--支持商业定制，提供自定义生成激活时间方法
setKEY({KEY = "fb197ea7dd4fd32703b15f5dbf75dbc2"}); -- 一机一码
```

**远程设置 HTTP post 请求 body**

名称|说明
-|-
API|函数名称
data|参数

```JSON
{  
	"API":"setKEY",  
	"data":{  
		"KEY":"fb197ea7dd4fd32703b15f5dbf75dbc2"
	}
}
```

激活许可函数：
* [MouseMove](#mousemove) 鼠标移动(相对坐标)
* [MouseDown](#mousedown) 鼠标按下
* [MouseUp](#mouseup) 鼠标抬起
* [MouseClick](#mouseclick) 鼠标单击/鼠标长按
* [MouseSlide](#mouseslide) 鼠标滑动
* [touchMove](#touchmove) 鼠标移动(绝对坐标)
* [KeyboardClick](#keyboardclick) 键盘输入

# setUART

### **设置鼠标配置**

函数说明：安卓手机、iOS13.5以下的手机点击、移动坐标出现不准确，鼠标无法使用等情况需要重新设置参数，使用该函数后会自动重启设备

玩法说明：
  - ESP8266 接线说明：引脚 D4 接 鼠标 RXD，5V 接 VCC，GND 接 GND
  - ESP32 接线说明：[工作室 ESP32 使用说明 - 接线图](https://docs.qq.com/doc/DVnR4amhRSGZZa1ZN)

参数|类型|说明
-|-|-
version|<font color=#FF8C00>string</font>|版本号
type|<font color=#FF8C00>string</font>|鼠标类型
system|<font color=#FF8C00>string</font>|根据手机系统类型切换USB鼠标协议：iOS/Android（"默认iOS"），蓝牙款不需要设置
BaudRate|<font color=#00ffff>number</font>|波特率
Proportion|<font color=#ffff00>table</font>|1号鼠标移动值比例 x(水平移动)、y(垂直移动)、z(滚轮转动)
Proportion_0|<font color=#ffff00>table</font>|0号鼠标移动值比例 x(水平移动)、y(垂直移动)、z(滚轮转动)
Proportion_2|<font color=#ffff00>table</font>|2号鼠标移动值比例 x(水平移动)、y(垂直移动)、z(滚轮转动)
KEY|<font color=#FF8C00>string</font>|设备激活码
TimeExpired|<font color=#FF8C00>string</font>|设备有效日期
Log|<font color=#00ff00>boolean</font>|全局日志模式 开启 = true/关闭 = false

**串口通讯**

```lua
--苹果系统 iOS13.5 - IOS15 1号鼠标默认比例值
setUART({ Proportion = { x = 2.0833333, y = 2.0833333, z = 1}});

--0号鼠标默认比例值
setUART({ Proportion_0 = { x = 1, y = 1, z = 1}});

--USB鼠标模式（波特率9600）
setUART({ type = "USB", BaudRate = 9600});

--蓝牙鼠标模式（波特率115200）
setUART({ type = "Bluetooth", BaudRate = 115200});

--全局关闭日志可扩展 0 号鼠标引脚控制
setUART({ Log = false });

--切换USB鼠标协议为Android系统，蓝牙鼠标无需设置该功能
setUART({ system = "Android" });
```

**关于 IOS 13.5 以下设置，仅供参考（以实际为准）**
*  `版本号：ESP8266 v1.0.3`  iOS13.3.1 鼠标点击不准确解决方案 [视频教程](https://www.bilibili.com/video/BV1oU4y1f7gb/?spm_id_from=333.788)
```lua
setUART({Proportion={x=4.4860381591079,y=7.979166539}});
--usb横屏6s IOS13.2 13.3  
setUART({Proportion={x=7.979166539,y=4.4860381591079}});
--usb竖屏6s IOS13.2 13.3  
   
setUART({Proportion={x=3.6298256540667,y=6.4562498967}});
--蓝牙横屏6s IOS13.2 13.3  
setUART({Proportion={x=6.4562498967,y=3.6298256540667}});
--蓝牙竖屏6s IOS13.2 13.3
```

**远程设置 HTTP post 请求 body**

名称|说明
-|-
API|函数名称
data|参数

```JSON
{  
	"API":"setUART",  
	"data":{  
		"Proportion": {
			"x": 2.0833333, 
			"y": 2.0833333, 
			"z": 1
		}
	}
}
```

远程关闭日志
```JSON
{  
	"API":"setUART",  
	"data":{  
		"Log": false
	}
}
```
# getWIFI

### **获取WIFI配置（ESP32专用）**

函数说明：以太网模式只支持100M网口，如果使用1000M网口，会导致ESP32重启，芯片发热，以太网模式一旦运行失败会自动切换为WIFI模式，WIFI模式下如果账号密码出错，则设备无法正常联网。

参数|类型|说明
-|-|-
version|<font color=#FF8C00>string</font>|版本号 WIFI
ssid|<font color=#FF8C00>string</font>|账号
pwd|<font color=#FF8C00>string</font>|密码
save|<font color=#00ff00>boolean</font>|保存账号密码到闪存(启动时自动连接速度快)
WIFI|<font color=#00ff00>boolean</font>|启动或关闭WIFI（启动以太网模式）
HTTPServer|<font color=#00ff00>boolean</font>|启动或关闭局域网服务（无法局域网内控制鼠标）
HTTPPost|<font color=#00ff00>boolean</font>|局域网监听端口(默认：80)
MQTTServer|<font color=#00ff00>boolean</font>|启动或关闭物联网服务（无法物联网远程控制鼠标）

**串口通讯**

```lua
--打印WIFI配置
ptable(getWIFI());
```

# getMQTTcode

### **获取物联网平台设备信息（ESP32专用）**

参数|类型|说明
-|-|-
device_id|<font color=#FF8C00>string</font>|设备MQTT服务器ID
key|<font color=#FF8C00>string</font>|设备MQTT服务器api-key

**串口通讯**

```lua
--打印物联网平台服务器设备信息
ptable(getMQTTcode());
```

# getMQTT

### **获取MQTT配置（ESP32专用）**

参数|类型|说明
-|-|-
version|<font color=#FF8C00>string</font>|版本号 MQTT
MQTT_Host|<font color=#FF8C00>string</font>|MQTT地址
MQTT_Port|<font color=#00ffff>number</font>|MQTT端口
topic|<font color=#ffff00>table</font>|`版本号：ESP32 v1.0.5` 订阅主题
register_code|<font color=#FF8C00>string</font>|设备注册码
Master_Apikey|<font color=#FF8C00>string</font>|产品api-key
Master_ID|<font color=#FF8C00>string</font>|产品ID

**串口通讯**

```lua
--打印MQTT配置信息
ptable(getMQTT());
```

# getUART

### **获取鼠标配置（ESP32专用）**

函数说明：全局日志占用一个鼠标接口(引脚名称：TX)，关闭全局日志输出可接入0号鼠标模块）

参数|类型|说明
-|-|-
version|<font color=#FF8C00>string</font>|版本号
type|<font color=#FF8C00>string</font>|鼠标类型
BaudRate|<font color=#00ffff>number</font>|波特率
Proportion|<font color=#ffff00>table</font>|1号鼠标移动值比例 x(水平移动)、y(垂直移动)、z(滚轮转动)
Proportion_0|<font color=#ffff00>table</font>|0号鼠标移动值比例 x(水平移动)、y(垂直移动)、z(滚轮转动)
Proportion_2|<font color=#ffff00>table</font>|2号鼠标移动值比例 x(水平移动)、y(垂直移动)、z(滚轮转动)
KEY|<font color=#FF8C00>string</font>|设备激活码
TimeExpired|<font color=#FF8C00>string</font>|设备有效日期
Log|<font color=#00ff00>boolean</font>|全局日志模式 开启 = true/关闭 = false

**串口通讯**

```lua
--打印鼠标配置信息
ptable(getUART());
```

# getESP

### **获取ESP芯片配置信息**

参数|类型|说明
-|-|-
Log|<font color=#00ff00>boolean</font>|全局日志模式 开启 = true/关闭 = false
HTTPPost|<font color=#00ffff>number</font>|局域网端口(默认：80)
KEY|<font color=#FF8C00>string</font>|设备激活码
_VERSION|<font color=#FF8C00>string</font>|版本号
GATEWAY|<font color=#FF8C00>string</font>|局域网网关
NETMASK|<font color=#FF8C00>string</font>|局域网子网掩码
MAC|<font color=#FF8C00>string</font>|ESP芯片MAC地址
_TYPE|<font color=#FF8C00>string</font>|ESP芯片类型
ID|<font color=#FF8C00>string</font>|ESP芯片ID
IP|<font color=#FF8C00>string</font>|局域网IP地址

**串口通讯**

```lua
--打印ESP芯片配置信息
ptable(getESP());
```

# iOS 鼠标设置与显示
- 新手解答：iOS设置指针跟踪速度可以降低鼠标指针像素移动之间的距离，跟踪速度会随着指针移动距离越大偏移越大，调到最低则可以接近每个像素点，可使用固定比例值。不设置跟踪速度需要动态调整比例，如果部分手机需要经常刷机重置，恢复出厂设置，可考虑动态调整比例值。详情看教程。
-  iOS13.5 - iOS15 (必须连接鼠标后才可以设置) [视频教程](https://www.bilibili.com/video/BV1EF411E7HN/?spm_id_from=333.788)

```
设置 → 通用 → 触摸板与鼠标 → 跟踪速度（乌龟 - 最低比例 = 2.0833333）
```

-  iOS13 - iOS13.4 (备注：不需要连接鼠标也可以设置)
```
设置 → 辅助功能 → 触控 → 辅助触控 → 跟踪速度
```

# Android 鼠标设置与显示
- 新手解答：安卓USB鼠标指针无法使用固定比例值。需要动态调整比例，详情看教程。
- 注意：一定不能连接USB鼠标！才会显示指针速度选项
```
备注：测试机型 小米8

设置 → 更多设置 → 语言与输入法 → 键盘、鼠标和触控板 → 指针速度（最低）

USB鼠标指针速度最低比例参考：

{1.坐标值，2.比例值}

Android_USB_BILI = {
	{0, 2}，{50, 2},
	{100, 2},{150, 1.66666},
	{200, 1.66666},{250, 1.42857},
	{300, 1.42857},{350, 1.25},
    {400, 1.25},{450, 1.25},
    {500, 1.25},{550, 1.25},
    {600, 1.25},{650, 1.23456},
    {700, 1.23456},{750, 1.21951},
    {800, 1.21951},{850, 1.20481},
    {900, 1.20481},{950, 1.19047},
    {1000, 1.19047},{1050, 1.17647},
    {1100, 1.17647},{1150, 1.16279},
    {1200, 1.16279},{1250, 1.19047},
    {1300, 1.19047},{1350, 1.18343},
    {1400, 1.18343},{1450, 1.17647},
    {1500, 1.17647},{1550, 1.13314},
    {1600, 1.13314},{1650, 1.17302},
    {1700, 1.17302},{1750, 1.17302},
    {1800, 1.17302},{1850, 1.17302},
    {1900, 1.17302},{1950, 1.17302},
    {2000, 1.17302},{2050, 1.17302},
    {2100, 1.17302},{2150, 1.17302},
    {2200, 1.17302},{2250, 1.17302},
    {2300, 1.17302},{2350, 1.17302},
    {2400, 1.17302},{2450, 1.17302},
    {2500, 1.17302},{2550, 1.17302},
    {2600, 1.17302},{2650, 1.17302}
}
```



# HTTPPost

**支持远程发送的命令API**

API|API 说明|data 类型|data 参数|Log 参数
-|-|-|-|-
MouseMove|相对移动|<font color=#FFFF00>词典</font>|x,y|默认:true
MouseClick|单击/长按|<font color=#FFFF00>词典</font>|sleep|默认:true
MouseSlide|绝对滑动|<font color=#FFFF00>词典</font>|x,y,x1,y1|默认:true
touchMove|绝对移动|<font color=#FFFF00>词典</font>|x,y|默认:true
KeyboardClick|键盘输入|<font color=#FFFF00>数组</font>|1、2、3、4..|默认:true
MouseDown|按下|<font color=#FFFF00></font>||默认:true
MouseUp|抬起|<font color=#FFFF00></font>||默认:true
setWIFI|设置 WIFI（注意：会重启设备）|<font color=#FFFF00>词典</font>|ssid,pwd|默认:true
setKEY|设置 激活码（注意：会重启设备）|<font color=#FFFF00>词典</font>|KEY|默认:true
setUART|设置 鼠标（注意：会重启设备）|<font color=#FFFF00>词典</font>|Proportion|默认:true
print|[版本号v1.0.5](#v1.0.5) 输出日志|<font color=#FF8C00>string</font>|"hello"|默认:true
ptable|[版本号v1.0.5](#v1.0.5) 输出日志(表)|<font color=#FFFF00>词典/数组</font>|[ "hello" , "hello2"]|默认:true


# 局域网HTTPPost

名称|说明
-|-
url|设备局域网IP地址
API|[API](#httppost) 函数名称
data|函数参数表
Log|[版本号v1.0.5](#v1.0.5) 接收数据并打印到串口，输出日志会影响效率，true = 输出，false = 关闭输出(提高效率)

**鱼叉助手(iOS免越狱) Post示例**

```lua
--iOS 免越狱用例
local http = require("socket.http")--需要下载socket 扩展库
local request_body = '{"API":"KeyboardClick","data":["l"]}' --键盘输入 l
local response_body = {}

local res, code, response_headers =
http.request {
	 url = "https://192.168.1.186", --局域网地址
	 method = "POST",
	 headers = {
	 ["Content-Length"] = #request_body,
	 },
	 source = ltn12.source.string(request_body),
	 sink = ltn12.sink.table(response_body)
	}
```

# 物联网HTTPPost

官网API文档 [OneNET - 中国移动物联网开放平台 (10086.cn)](https://open.iot.10086.cn/doc/v5/develop/detail/611)

参数返回码文档：[OneNET - 中国移动物联网开放平台 (10086.cn)](https://open.iot.10086.cn/doc/v5/develop/detail/577)

每个服务实例**每月**前2000万条消息免费使用。[收费标准](https://open.iot.10086.cn/doc/v5/develop/detail/216)


名称|说明
-|-
url|中国移动物联网平台(http://api.heclouds.com/cmds)
device_id|设备MQTT服务器ID（必填）
api-key|设备MQTT服务器api-key（必填）
API|[API](#httppost) 函数名称
data|函数参数表
Log|[版本号v1.0.5](#v1.0.5) 接收数据并打印到串口，输出日志会影响效率，true = 输出，false = 关闭输出(提高效率)

**网页端示例**
```lua
URL:
http://api.heclouds.com/cmds?device_id=887030162


Header:
api-key:       Roc3=Uqy3V0oBhA0j5DyztuuAsA=
Content-Type:  application/json

发送参数例子:(设备端接收成功不打印，提升效率)
{
	"API":"MouseMove",
	"data":{
		"x":100,
		"y":100
	},
	"Log":false
}      
发送参数例子:(设备端接收成功打印日志)
{
	"API":"KeyboardClick",
	"data":["a","b","c"]
}
发送参数例子:(设备端接收成功不打印日志，调用打印函数打印日志)
{
	"API":"print",
	"data":"hello",
	"Log":false
}    

设备在线接收成功返回码 erron = 0，离线返回 erron  = 10
```

**鱼叉助手(iOS免越狱)Post示例**

```lua
--iOS 免越狱用例
local http = require("socket.http")--需要下载socket 扩展库
local request_body = '{"API":"KeyboardClick","data":["l"]}' --键盘输入 l
local response_body = {}

mSleep(10000) --10秒后
for i = 1,10 do --发送10次命令
	local res, code, response_headers =
	 http.request {
		 url = "http://api.heclouds.com/cmds?device_id=123456782", --设备服务器ID
		 method = "POST",
		 headers = {
		 ["Content-Type"] = "application/json",
		 ["Content-Length"] = #request_body,
		 ["api-key"] = "Q=hxaabvcdfghSb4HvZM7fNX8=", --设备服务器api-key
		 },
		 source = ltn12.source.string(request_body),
		 sink = ltn12.sink.table(response_body)
	}
end
```

**触动精灵(越狱)Post示例**

```lua
--iOS 函数用例
local ts = require("ts")
for var = 1, 10 do
    -- body
    header_send = {
		typeget = "ios", 
		["api-key"] = "b=nfZAE3f3vHy=17QsveGLggcuw=" --设备api-key
	}
    body_send = '{"API":"MouseMove","data":{"x":-10,"y":0}}' --鼠标向左相对移动
    ts.setHttpsTimeOut(60)
    status_resp, header_resp, body_resp =
        ts.httpPost("http://api.heclouds.com/cmds?device_id=894617519", --设备服务器ID
		header_send, 
		body_send, 
		true
	)
    nLog(status_resp, 0)
    nLog(header_resp, 0)
    nLog(body_resp, 0)
end
```

# ESP8266更新日志
### ESP8266 v1.0.57
- 新增 ESP8266 MouseMove 滚轮功能，解决部分窗口无法滑动问题(使用滚轮滑动)

### ESP8266 v1.0.56
- 优化 ESP8266 MouseSlide 滑动函数不灵敏问题，稳定性提高

### ESP8266 v1.0.55
- 修复 ESP8266 激活验证码导致无法正常使用的问题

### ESP8266 v1.0.54
- 删除 getUART、getWIFI、getMQTT、getMQTTcode 函数优化内存
- 修复 ESP8266 内存不足导致重启的问题
- 修复 键盘问题

### ESP8266 v1.0.53
- 新增 ESP8266 支持 2 台 鼠标控制 (UART v1.0.3)
- 新增 ESP8266 日志全局关闭功能(UART v1.0.3)
- 修复 ESP8266 键盘复制粘贴出现重复2次的Bug (UART v1.0.3)

### ESP8266 v1.0.52
- 修复 ESP8266 内存不足，自定义订阅功能暂时关闭（MQTT v1.0.4）

### ESP8266 v1.0.5 - v1.0.51
- 更新 ESP8266 MQTT掉线自动重连（MQTT v1.0.3）
- 更新 ESP8266 MQTT日志打印输出功能（MQTT v1.0.3）

### ESP8266 v1.0.4
- 新增 ESP8266 激活码验证远程激活
- 更新 ESP8266 升级MQTT订阅功能（MQTT v1.0.2）
- 更新 ESP8266 文件名加密混淆
- 更新 ESP8266 固件系统API 不允许远程使用

### ESP8266 v1.0.3
- 更新 ESP8266 set相关函数改为同类型替换
- 修复 ESP8266 ==严重 bug== 修复无法设置MQTT订阅 （MQTT v1.0.1）
- 更新 ESP8266 激活码验证算法更新（AESECB v1.0.1）
- 更新 ESP8266 ==命名问题== （Mouse v1.0.0） 改名为 （UART v1.0.1）
- 修复 ESP8266 ==iOS13.3.1 bug== Proportion 数值 改为 表 { x = 2.0833333, y = 2.0833333 } （区分 x 和 y ）
- 修复 ESP8266 ==内存不足 bug== ESP8266 固件重新构建（`crypto` `encoder` `file` `gpio` `http` `mqtt` `node` `sjson` `tmr` `uart` `wifi`）

### ESP8266 v1.0.2
- 新增 ESP8266 MQTT协议（MQTT v1.0.0）
- 新增 ESP8266 蓝牙鼠标协议 Mouse API（Mouse v1.0.0）
- 新增 ESP8266 WIFI（WIFI v1.0.0）
- 新增 ESP8266 激活码验证（AESECB v1.0.0）

# ESP32更新日志
### ESP32 v1.0.55
- 新增 ESP32 setUART 设置鼠标函数 `system` 手机系统设置参数，可设置iOS/Android，默认iOS (UART v1.0.5)
- 新增 MouseDown 按下函数，MouseUp 松手函数安卓手机鼠标点击协议，解决安卓USB鼠标无法点击问题
- 新增 MouseMove 相对移动函数动态比例调参功能，解决安卓手机坐标比例问题
- 新增 MouseSlide 滑动函数、touchMove 绝对移动函数延时动态调参功能，可自定义延时时间

### ESP32 v1.0.54 
(同步 ESP8266 v1.0.57)
- 新增 ESP32 蓝牙鼠标 MouseMove 滚轮功能，解决部分窗口无法滑动问题(使用滚轮滑动)

### ESP32 v1.0.53
- 修复 ESP32 物联网后台数据流数据不显示的bug

### ESP32 v1.0.52
(同步 ESP8266 v1.0.56)
- 优化 ESP32 蓝牙鼠标 MouseSlide 滑动函数不灵敏问题，稳定性提高

### ESP32 v1.0.51
- 优化 ESP32 无以太网扩展模块导致以太网模式报错问题，自动切换WIFI模式
- 修复 ESP32 MQTT后台数据显示异常、串口日志没用打印MQTTcode参数等问题
- 新增 ESP32 可修改0号鼠标波特率（默认9600）
- 新增 ESP32 蓝牙鼠标模式/USB鼠标模式(双模式自定义)

### ESP32 v1.0.5
- 新增 ESP32 USB鼠标协议 Mouse API（UART v1.0.2） 
- 新增 ESP32 固件（`crypto` `encoder` `eth` `file` `gpio` `http` `mqtt` `net` `node` `sjson` `time` `tmr` `uart` `wifi` ）
- 新增 ESP32 以太网 WIFI 双模式（WIFI v1.0.1）
- 新增 ESP32 局域网 物联网 双模式 (MQTT v1.0.3)
- 新增 ESP32 支持 3 台 鼠标控制 (UART v1.0.3)
- 新增 ESP32 日志全局关闭功能(UART v1.0.3)
- 新增 ESP32 鼠标移动比例3台鼠标自定义(UART v1.0.4)
- 更新 ESP32 MQTT掉线自动重连（MQTT v1.0.3）
- 更新 ESP32 MQTT日志打印输出功能（MQTT v1.0.3）
- 更新 ESP32 文件名加密混淆
- 更新 ESP32 激活码验证
- 优化 ESP32 Mouse API 键盘指令功能
- 优化 ESP32 MouseMove 算法，提高移动效率
- 修复 ESP32 以太网重启后无法连接问题